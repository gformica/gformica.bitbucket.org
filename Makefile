all:
	@echo "Usage: make server|install"
	@echo ""

server:
	open "http://127.0.0.1:4000/"
	jekyll s

install:
	jekyll b
	yes | cp -av _site/* ..
	rm -rf _site/*